
const express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios');
const secretKey = '6LeJ5v8UAAAAAEWl8xUwdlp9cNfVpoUSf7b9i_hw';

const app = express();

app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

app.set('views', __dirname + '/views')
app.set('view engine', 'ejs')

app.get('/',(req,res) =>{
    res.render('index');
    console.log('at home page');
}); 



app.post('/verify',(req,res)=>{
    
    if(!req.body.captcha){
        console.log("captcha token not found");
        return res.json({"success": false, "msg":"Captcha is not checked"});
       
    }

    const verifyUrl = `https://www.google.com/recaptcha/api/siteverify?secret=${secretKey}&response=${req.body.captcha}`;

    axios.post(verifyUrl)
      .then(function (body) {

        if(!body.data.success && body.data.success === undefined){
            console.log("captcha verification failed")
            return res.json({"success":false, "msg":"captcha verification failed"});
        }
        else if(body.data.score < 0.5){
            console.log("you might be a bot, sorry!")
            return res.json({"success":false, "msg":"you might be a bot, sorry!", "score": body.data.score});
        }
            console.log("captcha verification passed")
            return res.json({"success":true, "msg":"captcha verification passed", "score": body.data.score});
      })
      .catch(function (error) {
        console.log(error);
      });

});


app.listen(3000,() =>{
    console.log('server is now up!');
});
